/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80019
Source Host           : localhost:3306
Source Database       : fp_lottery2

Target Server Type    : MYSQL
Target Server Version : 80019
File Encoding         : 65001

Date: 2022-01-02 22:42:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for fp_appinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_appinfo`;
CREATE TABLE `fp_appinfo` (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
  `name` varchar(200) DEFAULT NULL,
  `apppath` varchar(255) DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `appkey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `intro` text,
  `version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `target` varchar(50) DEFAULT NULL,
  `adminindex` varchar(255) DEFAULT NULL,
  `apptype` varchar(60) DEFAULT NULL COMMENT '是否系统应用',
  `datetime` datetime DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fp_appinfo
-- ----------------------------
INSERT INTO `fp_appinfo` VALUES ('14819286672786432', '方配后台管理系统', '/admin/', '方配', 'fpadmin.png', 'FPAdmin', '提供方配.Core后台管理系统，官方网站：http://www.fangpage.com', '1.0.0', '', '', 'system', '2021-04-18 16:00:14', '2021-10-25 14:18:04');
INSERT INTO `fp_appinfo` VALUES ('14819288422794240', '系统用户管理', '/user/', '方配', '', 'FPUser', '提供方配.Core后台用户管理，官方网站：http://www.fangpage.com', '1.0.0', '', '', 'system', '2021-04-19 13:07:58', '2021-06-08 09:24:36');
INSERT INTO `fp_appinfo` VALUES ('15346034237195264', 'Font Awesome', '/plugins/font-awesome/', 'FontAwesome', '', 'FontAwesome', '一套绝佳的图标字体库和CSS框架', '4.7.0', '', 'https://fontawesome.dashgame.com', 'plugin', '2021-06-24 19:53:19', '2021-06-25 12:05:37');
INSERT INTO `fp_appinfo` VALUES ('15555314795611136', '摇号系统', '/lottery/', '方配', '', 'FPLottery', '', '1.0.0', '', '', '', '2021-11-19 16:04:30', '2021-11-23 13:26:00');
INSERT INTO `fp_appinfo` VALUES ('15581195701306368', '类型管理', '/type/', '方配', '', 'FPType', '', '1.0.0', '', '', 'user', '2021-12-07 22:51:55', '2021-12-07 22:53:02');

-- ----------------------------
-- Table structure for fp_companyinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_companyinfo`;
CREATE TABLE `fp_companyinfo` (
  `id` varchar(60) NOT NULL,
  `typeid` varchar(60) DEFAULT NULL,
  `number` int DEFAULT NULL,
  `companyname` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `address` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `faren` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `linker` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `tel` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `fuli` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `fuli_name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `counts` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of fp_companyinfo
-- ----------------------------
INSERT INTO `fp_companyinfo` VALUES ('15617984457393152', '15581815062250496', '1', '测试公司1', '测试公司1', '测试公司1', '测试公司1', '33225', 'ewreere', 'erer', '下浮率', '0', '1');
INSERT INTO `fp_companyinfo` VALUES ('15617987398140928', '15581815062250496', '2', '测试公司2', '测试公司2', '测试公司2', '测试公司2', '324234', '2324324', '324', '下浮率', '1', '2');
INSERT INTO `fp_companyinfo` VALUES ('15617988005921792', '15581815062250496', '3', '测试公司3', '测试公司3', '测试公司3', '测试公司3', '测试公司3', '32423432', '32423', '下浮率', '0', '1');

-- ----------------------------
-- Table structure for fp_department
-- ----------------------------
DROP TABLE IF EXISTS `fp_department`;
CREATE TABLE `fp_department` (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parentid` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `shortname` varchar(255) DEFAULT NULL,
  `depth` int DEFAULT NULL,
  `display` int DEFAULT NULL,
  `status` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fp_department
-- ----------------------------
INSERT INTO `fp_department` VALUES ('1', null, 'dfgg', 'sdfdsff', null, null, null);

-- ----------------------------
-- Table structure for fp_lotteryconfirm
-- ----------------------------
DROP TABLE IF EXISTS `fp_lotteryconfirm`;
CREATE TABLE `fp_lotteryconfirm` (
  `id` varchar(60) NOT NULL,
  `lotteryid` varchar(60) DEFAULT NULL,
  `logid` varchar(60) DEFAULT NULL,
  `sendcom` varchar(100) DEFAULT NULL,
  `sendtime` varchar(60) DEFAULT NULL,
  `sender` varchar(60) DEFAULT NULL,
  `projectname` varchar(100) DEFAULT NULL,
  `material` varchar(255) DEFAULT NULL,
  `receiver` varchar(60) DEFAULT NULL,
  `tel` varchar(60) DEFAULT NULL,
  `lotter` varchar(60) DEFAULT NULL,
  `lottedate` varchar(60) DEFAULT NULL,
  `banli` varchar(100) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fp_lotteryconfirm
-- ----------------------------

-- ----------------------------
-- Table structure for fp_lotteryinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_lotteryinfo`;
CREATE TABLE `fp_lotteryinfo` (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `typeid` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `type_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `company_list` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `companys` int DEFAULT NULL,
  `lotnum` int DEFAULT NULL,
  `luckynum` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `buyer` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `lun` int(10) unsigned zerofill DEFAULT NULL,
  `ci` int(10) unsigned zerofill DEFAULT NULL,
  `ltdate` datetime DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of fp_lotteryinfo
-- ----------------------------
INSERT INTO `fp_lotteryinfo` VALUES ('15617989148230656', '15581815062250496', '房屋建筑及装修装饰工程', '房屋建筑及装修装饰工程', '1,2,3', '3', '1', '2', '水电费', '0000000000', '0000000001', '2022-01-02 22:40:09', '2');
INSERT INTO `fp_lotteryinfo` VALUES ('15617989675402240', '15581815062250496', '', '测试', '', '2', '1', '', '是的', '0000000000', '0000000002', '2022-01-02 22:40:41', '1');

-- ----------------------------
-- Table structure for fp_lotterylog
-- ----------------------------
DROP TABLE IF EXISTS `fp_lotterylog`;
CREATE TABLE `fp_lotterylog` (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lotteryid` varchar(60) DEFAULT NULL,
  `buyer` varchar(60) DEFAULT NULL,
  `project_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `typeid` varchar(60) DEFAULT NULL,
  `typename` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `number` int DEFAULT NULL,
  `companyname` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `faren` varchar(60) DEFAULT NULL,
  `linker` varchar(60) DEFAULT NULL,
  `tel` varchar(60) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `fuli_name` varchar(60) DEFAULT NULL,
  `fuli` varchar(60) DEFAULT NULL,
  `lun` int(10) unsigned zerofill DEFAULT NULL,
  `ci` int(10) unsigned zerofill DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fp_lotterylog
-- ----------------------------
INSERT INTO `fp_lotterylog` VALUES ('15617989363663872', '15617989148230656', '水电费', '房屋建筑及装修装饰工程', '15581815062250496', '房屋建筑及装修装饰工程', '2', '测试公司2', '', '测试公司2', '测试公司2', '324234', '2324324', '下浮率', '324', '0000000000', '0000000001', '2022-01-02 22:40:22');

-- ----------------------------
-- Table structure for fp_menuinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_menuinfo`;
CREATE TABLE `fp_menuinfo` (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parentid` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `markup` varchar(100) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `depth` int DEFAULT NULL,
  `display` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fp_menuinfo
-- ----------------------------
INSERT INTO `fp_menuinfo` VALUES ('14816339144819712', '15555233462535168', '主页', 'homeindex', 'layui-icon layui-icon-home', 'sys_config.html', '0', '1', '1');
INSERT INTO `fp_menuinfo` VALUES ('14817755636073472', '15555233462535168', '系统设置', '', 'layui-icon layui-icon-set', '', '0', '2', '1');
INSERT INTO `fp_menuinfo` VALUES ('14817838928593920', '15555233462535168', '用户设置', '', 'layui-icon layui-icon-user', '', '0', '3', '1');
INSERT INTO `fp_menuinfo` VALUES ('14817847193945088', '14817755636073472', '菜单管理', null, '', '/admin/menu_list.html', '0', '3', '1');
INSERT INTO `fp_menuinfo` VALUES ('14817878472803328', '14817838928593920', '角色管理', null, '', '/user/role_list.html', '0', '2', '1');
INSERT INTO `fp_menuinfo` VALUES ('14819214596637696', '14817838928593920', '部门管理', null, '', '/user/depart_list.html', '0', '1', '1');
INSERT INTO `fp_menuinfo` VALUES ('14819291942732800', '14817838928593920', '用户管理', null, '', '/user/user_list.html', '0', '4', '1');
INSERT INTO `fp_menuinfo` VALUES ('14821765541135360', '14817755636073472', '应用管理', null, '', '/admin/app_manage.html', '0', '2', '1');
INSERT INTO `fp_menuinfo` VALUES ('14829225151841281', '14817755636073472', '通用设置', null, '', '/admin/sys_config.html', '0', '1', '1');
INSERT INTO `fp_menuinfo` VALUES ('15555233462535168', '', '系统管理', '', 'layui-icon layui-icon-set', '', '0', '1', '1');
INSERT INTO `fp_menuinfo` VALUES ('15555692273959936', '15555233462535168', '摇号管理', '', 'layui-icon layui-icon-user', '', '0', '4', '1');
INSERT INTO `fp_menuinfo` VALUES ('15555693560284160', '15555692273959936', '定点单位管理', '', '', '/lottery/company_list.html', '0', '2', '1');
INSERT INTO `fp_menuinfo` VALUES ('15558398686151680', '15555692273959936', '工程项目管理', '', '', '/lottery/lottery_list.html', '0', '3', '1');
INSERT INTO `fp_menuinfo` VALUES ('15576860112552960', '15555692273959936', '项目类型管理', '', '', '/type/type_list.html', '0', '1', '1');
INSERT INTO `fp_menuinfo` VALUES ('15597866789045248', '15555692273959936', '中标查询统计', '', '', '/lottery/lottery_company.html', '0', '4', '1');

-- ----------------------------
-- Table structure for fp_roleinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_roleinfo`;
CREATE TABLE `fp_roleinfo` (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `isadmin` varchar(255) DEFAULT NULL,
  `departs` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `menus` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `status` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fp_roleinfo
-- ----------------------------
INSERT INTO `fp_roleinfo` VALUES ('14819199099962368', '超级管理员', 'super', '', '1', '14819286672786432,14819288578327552', '15555233462535168,14816339144819712,14817755636073472,14829225151841281,14821765541135360,14817847193945088,14817838928593920,14819214596637696,14817878472803328,14819291942732800,15555692273959936,15576860112552960,15555693560284160,15558398686151680,15597866789045248', '1');
INSERT INTO `fp_roleinfo` VALUES ('15602325213889536', '快快快', '', '', '1', '', '15555233462535168,15555692273959936,15576860112552960,15555693560284160,15558398686151680,15597866789045248', '0');

-- ----------------------------
-- Table structure for fp_typeinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_typeinfo`;
CREATE TABLE `fp_typeinfo` (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parentid` varchar(60) DEFAULT NULL,
  `typename` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `markup` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `note` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `display` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of fp_typeinfo
-- ----------------------------
INSERT INTO `fp_typeinfo` VALUES ('15581749120713728', '', '定点施工单位', '', '', '1');
INSERT INTO `fp_typeinfo` VALUES ('15581815062250496', '15581749120713728', '房屋建筑及装修装饰工程', '', '', '1');
INSERT INTO `fp_typeinfo` VALUES ('15598346276668416', '15581749120713728', '市政公用工程', '', '', '2');
INSERT INTO `fp_typeinfo` VALUES ('15598359581852672', '15581749120713728', '公路桥梁工程', '', '', '3');
INSERT INTO `fp_typeinfo` VALUES ('15598370695070720', '15581749120713728', '水利工程', '', '', '4');
INSERT INTO `fp_typeinfo` VALUES ('15598374892389376', '15581749120713728', '电气设备安装（高压配电）工程', '', '', '5');
INSERT INTO `fp_typeinfo` VALUES ('15598379277222912', '15581749120713728', '环境保护工程', '', '', '6');
INSERT INTO `fp_typeinfo` VALUES ('15598385189880832', '15581749120713728', '通信工程', '', '', '7');
INSERT INTO `fp_typeinfo` VALUES ('15599534674199552', '', '定点服务单位', '', '', '2');
INSERT INTO `fp_typeinfo` VALUES ('15599536292873216', '15599534674199552', '工程勘察服务', '', '', '1');
INSERT INTO `fp_typeinfo` VALUES ('15599546770703360', '15599534674199552', '房屋建筑及装修工程设计服务', '', '', '2');
INSERT INTO `fp_typeinfo` VALUES ('15599547175404544', '15599534674199552', '市政公用工程设计服务', '', '', '3');
INSERT INTO `fp_typeinfo` VALUES ('15599548280587264', '15599534674199552', '公路桥梁工程设计服务', '', '', '4');
INSERT INTO `fp_typeinfo` VALUES ('15599548727772160', '15599534674199552', '水利水电工程设计服务', '', '', '5');
INSERT INTO `fp_typeinfo` VALUES ('15600992172082176', '15599534674199552', '房屋建筑及装修工程监理服务', '', '', '6');
INSERT INTO `fp_typeinfo` VALUES ('15600998081692672', '15599534674199552', '市政公用工程监理服务', '', '', '7');
INSERT INTO `fp_typeinfo` VALUES ('15601064828797952', '15599534674199552', '公路桥梁工程监理服务', '', '', '8');
INSERT INTO `fp_typeinfo` VALUES ('15601071584969728', '15599534674199552', '水利水电工程监理服务', '', '', '9');
INSERT INTO `fp_typeinfo` VALUES ('15601075992871936', '15599534674199552', '工程常规检测服务', '', '', '10');
INSERT INTO `fp_typeinfo` VALUES ('15601083592328192', '15599534674199552', '施工设计图审查服务', '', '', '11');

-- ----------------------------
-- Table structure for fp_userinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_userinfo`;
CREATE TABLE `fp_userinfo` (
  `id` varchar(60) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `headimg` varchar(255) DEFAULT NULL,
  `realname` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `roleid` varchar(255) DEFAULT NULL,
  `departid` varchar(255) DEFAULT NULL,
  `openid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `status` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fp_userinfo
-- ----------------------------
INSERT INTO `fp_userinfo` VALUES ('14820260937352192', 'admin', 'e10adc3949ba59abbe56e057f20f883e', null, '管理员', '男', '13481092834', '', '', '14819199099962368', '1', '0', '', '1');
